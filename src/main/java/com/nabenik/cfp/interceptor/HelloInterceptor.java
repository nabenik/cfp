package com.nabenik.cfp.interceptor;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

/**
 *
 * @author tuxtor
 */
public class HelloInterceptor {
    
    @AroundInvoke
    public Object trace(InvocationContext invocationContext) throws Exception{
        long start = System.nanoTime();
        try{
            return invocationContext.proceed();
        }finally{
            long executionTime = (System.nanoTime() - start);
            System.out.println("Metodo: " + invocationContext.getMethod() + " ejecutado a las: " + executionTime + " ns");
        }
    }
}

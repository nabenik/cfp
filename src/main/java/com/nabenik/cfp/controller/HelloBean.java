package com.nabenik.cfp.controller;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateful;

@Stateful
public class HelloBean implements Serializable {

    private static final long serialVersionUID = -1L;
    
    private int counter = 0;
        
    public String doHello(){
        Logger.getLogger(HelloBean.class.getName())
            .log(Level.INFO, "Running on: {0}",
                System.getProperty("com.sun.aas.instanceName"));

        counter++;
        return "Hello user today is " + (new Date()).toString() + " counter value " + counter + "Running on: {0}" +
                System.getProperty("com.sun.aas.instanceName");
    }
    
    /*
    * O(2^n) fibonacci implementation
    */
    public long doFibonacci(int n) {
        if (n <= 1) 
            return n;
        else 
            return doFibonacci(n-1) + doFibonacci(n-2);
    }
    
    
    public String doMemload(Long it) {
        SecureRandom random = new SecureRandom();
        StringBuilder finalBuilder = new StringBuilder();
        List<String> dumbResultList = new ArrayList<>();
        
        
        if(it == null) it = 1000L;
        
        try {
            while(it > 0){
                dumbResultList.add(new BigInteger(130, random).toString(32));
                it--;
            }
        } catch (Exception e) {
        }
        dumbResultList.forEach(x -> finalBuilder.append(x));
        
        
        return finalBuilder.toString();
                
    }
        
        
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik.cfp.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Stateful;
import javax.mail.Session;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author tuxtor
 */
@Stateful
public class EmailSenderEJB {

    private static final Logger LOG = Logger.getLogger(EmailSenderEJB.class.getName());
    
    private static final String CONTENT_TYPE_KEY = "Content-Type";
    
    private static final String CONTENT_TYPE_VALUE = "text/html;charset=UTF-8";
    
    private static final String CHARSET = "UTF-8";
    
    @Resource(name = "mail/nabenik")
    private Session session;
    
    @Asynchronous
    public void sendEmailMessage(String from, List<String> toList, String subject,
            HashMap<String, Object> markerParameters) {
        String templateUri = "template-default.ftl";
        
        LOG.log(Level.INFO, "templateUri:  {0}", templateUri);
        
        this.sendEmailMessage(from, toList, subject, markerParameters, templateUri);
    }
    
    @Asynchronous
    public void sendEmailMessage(String from, List<String> toList, String subject,
            HashMap<String, Object> markerParameters, String templateURI) {
        try {
            LOG.info("Creating body");
            String body = "<h1>Hola</h1>";
            
            LOG.info("Creating message");
            MimeMessage msg = new MimeMessage(session);
            msg.setHeader(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);
            
            msg.setHeader("Content-Transfer-Encoding", "base64");
            
            msg.setFrom(new InternetAddress(from));
            
            Address[] addresses = new Address[toList.size()];
            
            for (int i = 0; i < toList.size(); i++) {
                addresses[i] = new InternetAddress(toList.get(i));
            }
            msg.setRecipients(Message.RecipientType.TO, addresses);
            msg.setSubject(subject, CHARSET);
            msg.setContent(body, MediaType.TEXT_HTML);
            
            LOG.info("Message was created");
            Transport.send(msg);
            
            LOG.info("Transport was sent");
        } catch (MessagingException  ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }
    
    /**
     * Returns the correct sender email address
     * @return
     */
    public String getDefaultSenderMessage() {
        return "no-reply@nabenik.com";        
    }
    
    /**
     * Returns the correct email address to receive the email
     * @return
     */
    public List<String> getToAddressMessage() {
        List<String> toList = new ArrayList ();
        toList.add("info@nabenik.com");
        return toList;        
    }
}

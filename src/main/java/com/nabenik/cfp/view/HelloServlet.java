
package com.nabenik.cfp.view;

import com.nabenik.cfp.controller.HelloBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tuxtor
 */
@WebServlet(name = "HelloServlet", urlPatterns = {"/helloServlet"})
public class HelloServlet extends HttpServlet {

    //@EJB
    //HelloBean helloBean;
    
    private static final String HELLO_BEAN_SESION_KEY 
    = "hello";
    
    //java:global/cfp/HelloBean!com.nabenik.cfp.controller.HelloBean
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // Obtain the EJB from the HTTP session
    HelloBean helloBean = 
      (HelloBean) request.getSession()
        .getAttribute(HELLO_BEAN_SESION_KEY);
    
    if(helloBean == null){
      // EJB is not present in the HTTP session
      // so let's fetch a new one from the container
      try {
        InitialContext ic = new InitialContext();
        helloBean = (HelloBean) 
         ic.lookup("java:global/cfp/HelloBean!com.nabenik.cfp.controller.HelloBean");
        
        // put EJB in HTTP session for future servlet calls
        request.getSession().setAttribute(
          HELLO_BEAN_SESION_KEY, 
          helloBean);
        
      } catch (Exception e) {
          e.printStackTrace();
          Logger.getLogger(StringServlet.class.getName()).log(Level.SEVERE, e.toString());
        
      }
    }
    
        
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.println("Hello");
            out.println(helloBean.doHello());
        } catch (IOException ex) {
            ex.printStackTrace();
          Logger.getLogger(StringServlet.class.getName()).log(Level.SEVERE, ex.toString());
        } finally {
            out.close();
        }
    }

}

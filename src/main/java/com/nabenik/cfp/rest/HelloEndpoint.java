package com.nabenik.cfp.rest;

import com.nabenik.cfp.controller.HelloBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/hello")
@Produces(MediaType.TEXT_PLAIN)
public class HelloEndpoint {
    
    @EJB
    HelloBean helloBean;

    @GET
    public Response doGet() {
            return Response.ok(helloBean.doHello()).build();
    }
    
    @GET
    @Path("/jndi")
    public String doJndiLookup() {
        InitialContext ic;
        try {
            ic = new InitialContext();
            return (String) ic.lookup("custom/dresponse");
        } catch (NamingException ex) {
            Logger.getLogger(MailResource.class.getName()).log(Level.SEVERE, null, ex);
            return "ok";
        }
    }
    
    @GET
    @Path("/fibonacci")
    public String doJndiLookup(@QueryParam("iterations") int iterations) {
        return String.valueOf(helloBean.doFibonacci(iterations));
    }
    
    @GET
    @Path("/membomb")
    public String doMembomb(@QueryParam("iterations") Long iterations) {
        return helloBean.doMemload(iterations);
    }
}
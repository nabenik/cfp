/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik.cfp.rest;

import com.nabenik.cfp.controller.EmailSenderEJB;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.enterprise.context.RequestScoped;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author tuxtor
 */
@Path("/domail")
@RequestScoped
public class MailResource {
    
    @EJB
    private EmailSenderEJB emailSender;

    /**
     * Creates a new instance of MailResource
     */
    public MailResource() {
    }

    /**
     * Retrieves representation of an instance of com.nabenik.cfp.rest.MailResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("text/json")
    public String getJson() {
        List<String> toList = new ArrayList<>();
                toList.add("vorozco@nabenik.com");
        emailSender.sendEmailMessage("no-reply@nabenik.com", toList, "prueba", null);
        
        InitialContext ic;
        try {
            ic = new InitialContext();
            return (String) ic.lookup("custom/dresponse");
        } catch (NamingException ex) {
            Logger.getLogger(MailResource.class.getName()).log(Level.SEVERE, null, ex);
            return "ok";
        }
    }

    /**
     * PUT method for updating or creating an instance of MailResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}

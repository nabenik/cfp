/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik.cfp.rest.service;

import com.nabenik.cfp.model.Speaker;
import com.nabenik.cfp.view.SpeakerBean;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 *
 * @author tuxtor
 */
@Stateless
@Path("/speaker")
public class SpeakerFacadeREST {
    
    @EJB
    SpeakerBean speakerBean;

   
    @GET
    @Produces("text/json")
    public List<Speaker> findAll() {
        return speakerBean.getAll();
    }
    
}
